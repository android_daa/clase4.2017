package cl.duoc.probandolosintent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class FormularioJuegosActivity extends AppCompatActivity {

    private EditText etUrlImagen;
    private ImageView ivPreview;
    private Button btnVerImagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_juegos);

        etUrlImagen = (EditText)findViewById(R.id.etUrlImagen);
        ivPreview = (ImageView)findViewById(R.id.ivPreview);
        btnVerImagen = (Button)findViewById(R.id.btnVerImagen);
        btnVerImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen(etUrlImagen.getText().toString());
            }
        });
    }
    private void cargarImagen(String url) {
        Picasso.with(this).load(url).into(ivPreview);
    }
}
